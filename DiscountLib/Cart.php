<?php
namespace DiscountLib;

use DiscountLib\Product;
use DiscountLib\Product\Set;

class Cart implements \Iterator{
    private $products = array();

    public function push(Product $product){
       $this->products[] = $product;
    }

    public function removeProduct($product){
        if( !($product instanceof Product)){
            return $this->removeProduct( $this->getProduct($product) );
        }
        else if(($key = array_search($product,$this->products,true)) !== false){
                $this->products = array_splice($this->products, $key,1)
                return $product;
        }

        return false;
    }

    public function count(){
      return count($this->products);
    }

    public function rewind(){
        reset($this->products);
    }
  
    public function current(){
        return current($this->products);
    }
  
    public function key(){
        return key($this->products);
    }
  
    public function next(){
        return next($this->products);
    }
  
    public function valid(){
        $key = key($this->products);
        return ($key !== NULL && $key !== FALSE);
    }
}
