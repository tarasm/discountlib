<?php
namespace DiscountLib\Product;
use DiscountLib\Product;

class Set {
   public $product;
   public $number;

   function __construct( \DiscountLib\Product $product, $number ){
      $this->product = $product;
      $this->number  = $number;
   }

   public function __toString(){
      return $this->product->name . "(". $this->product->id . "): " . $this->number;
   }
}
