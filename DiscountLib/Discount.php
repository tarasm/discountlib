<?php

  namespace DiscountLib;

  class Discount {
    private $fail;
    private $stop = false;
    private $cart;
    private $currCart;

    function __construct(Cart $cart){
      $this->cart = $cart;
    }

    public function start(){
       if($this->stop){
          $this->fail = true;
          return $this;
       };

       $this->fail = false;
       $this->currCart = clone $this->cart;
       return $this;
    }

  }