<?php

namespace DiscountLib;

class Product {
  public $id;
  public $name;
  public $price;
  public $currPrice;

  function __construct($id, $name, $price){
     $this->id    = $id;
     $this->name  = $name;
     $this->price = $price;
     $this->currPrice = $price;
  }
}
