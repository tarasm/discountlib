<?php
  namespace DiscountLib\Discount\Clause;
  use DiscountLib\Discount\Clause;
  use DiscountLib\Cart;

  class MixProduct  extends DiscountLib\Discount\Clause {
    private $selectedProduct;

    function __construct(){
       $this->selectedProduct = new DiscountLib\Cart();
       parent::__construct();
    }

    public function retrive($product){
       if($this->fail) return $this;

       if($product = $this->currCart->removeProduct($product)){
          $this->selectedProduct->push( $product );
       } else {
          $this->fail = true;
       };

       return $this;
    }
  }
    