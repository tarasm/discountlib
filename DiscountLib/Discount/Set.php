<?php
  namespace DiscountLib\Discount;

  abstract class Set implements \Iterator {
     public const TYPE_ABSOLUTE = 1;
     public const TYPE_PERCENT  = 2;

     public $type;
     public $conditions;

     
     public function rewind(){
          reset($this->conditions);
     }
  
     public function current(){
        return current($this->conditions);
     }
  
     public function key(){
        return key($this->conditions);
     }
  
     public function next(){
        return next($this->conditions);
     }
  
     public function valid(){
        $key = key($this->conditions);
        return ($key !== NULL && $key !== FALSE);
     }

  }